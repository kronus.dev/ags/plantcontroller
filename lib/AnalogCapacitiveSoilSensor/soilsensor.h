/*
 * Capacitive Soil Moisture Sensor Interface
 * 
 * Author: Mathieu Dugas
 * License: MIT
 */
#ifndef SOILSENSOR_H
#define SOILSENSOR_H

class SoilSensor {
    public:
        SoilSensor(int calibration [], int pin, int refresh);
        int getMoisture();
        Ticker ticker;

    private:
        int dry;
        int wet;
        int pin;
        int value;
        void readSensor();
        
};

#endif /* SOILSENSOR_H */