/*
 * Capacitive Soil Moisture Sensor Interface
 * 
 * Author: Mathieu Dugas
 * License: MIT
 */
#ifndef SOILSENSOR
#define SOILSENSOR

#include <Arduino.h>
#include <Ticker.h>
#include "soilsensor.h"

SoilSensor::SoilSensor(int calibration [], int pin, int refresh) : dry(calibration[1]), wet(calibration[0]), pin(pin), ticker{[this](){this->readSensor();}, 1000, 0, MILLIS}{
    ticker.start();
};

void SoilSensor::readSensor(){
    int sensorValue = analogRead(this -> pin);
    this -> value = (float)(sensorValue - this -> wet) / (float)(this -> dry - this -> wet) * 100;
};

int SoilSensor::getMoisture(){
    return this -> value;
}

#endif /* SOILSENSOR */