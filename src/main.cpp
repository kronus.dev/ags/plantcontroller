#include <Arduino.h>
#include "soilsensor.h"

#include "main.h"

SoilSensor sensor(calibration, A0, 500);

void setup() {
  sensor.ticker.update();
}

void loop() {
  // put your main code here, to run repeatedly:
}